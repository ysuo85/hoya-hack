package com.example.jieliang.veteran;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Home extends AppCompatActivity {

    TextView signUpTextview;
    EditText usernameEditText;
    EditText pEditText;
    Button logInButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        signUpTextview = (TextView) findViewById(R.id.signUpTextview);
        //underline text
        SpannableString content = new SpannableString("Sign Up Now!");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        signUpTextview.setText(content);

        signUpTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this, Register.class);
                startActivity(i);
            }
        });

        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        usernameEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernameEditText.setText("");
            }
        });

        pEditText = (EditText) findViewById(R.id.pEditText);
        pEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pEditText.setText("");
            }
        });

        logInButton = (Button) findViewById(R.id.logInButton);
        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 1/18/16  communicate with backend using the user information & go to content activity
            }
        });
    }
}
